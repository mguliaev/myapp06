package com.example.misaki.myapp06;

import android.support.annotation.NonNull;

import java.util.Date;

public class Operation {
    private int contractNumber;
    private long id;
    @NonNull
    private Date date;
    @NonNull
    private String merchantName;
    @NonNull
    private String description;
    private double amount;
    //@CurrencyType
    private int currency;

    public Operation(int contractNumber, long id, @NonNull Date date, @NonNull String merchantName,
                     @NonNull String description, double amount, int currency) {
        this.contractNumber = contractNumber;
        this.id = id;
        this.date = date;
        this.merchantName = merchantName;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
    }

    public int getContractNumber() {
        return contractNumber;
    }

    public long getId() {
        return id;
    }

    @NonNull
    public Date getDate() {
        return date;
    }

    @NonNull
    public String getMerchantName() {
        return merchantName;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public double getAmount() {
        return amount;
    }

    public int getCurrency() {
        return currency;
    }

    String currencyToString(int currency) {
        switch (currency) {
            case 1: return "руб.";
            case 2: return "euro";
            case 3: return "$";
            default: return "";
        }
    }

    public String toString() {
        return "Operation: id:" + Long.toString(id)
                + " Контракт:" + Integer.toString(contractNumber)
                + " Имя:" + merchantName
                + " Описание:" + description
                + " Сумма:" + Double.toString(amount)
                + currencyToString(currency)
                + " Дата:" + date.toString();
    }
}
