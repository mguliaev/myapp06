package com.example.misaki.myapp06;

import android.graphics.Path;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OperationsRepositoryTest {

    protected OperationsRepository operationsRepository = new OperationsRepository();
    protected List<Operation> operationList = new ArrayList<>();
    protected SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");

    OperationsRepositoryTest() {
        try {
            operationList.add(new Operation(1, 1, format.parse("2018-12-01"),
                    "Магнит", "Продажа", 201, 1));
            operationList.add(new Operation(2, 2, format.parse("2018-12-10"),
                    "Оптима", "Продажа", 202, 1));
            operationList.add(new Operation(3, 3, format.parse("2018-02-17"),
                    "Магнит", "Продажа", 203, 1));
            operationList.add(new Operation(4, 4, format.parse("2018-11-11"),
                    "Айкай", "Продажа", 204, 1));
            operationList.add(new Operation(5, 5, format.parse("2018-03-01"),
                    "Магнит", "Продажа", 205, 1));
            operationList.add(new Operation(5, 6, format.parse("2018-10-03"),
                    "Магнит", "Продажа", 206, 1));
            operationList.add(new Operation(4, 7, format.parse("2018-04-01"),
                    "Магнит", "Продажа", 207, 1));
            operationList.add(new Operation(3, 8, format.parse("2018-09-08"),
                    "Айкай", "Продажа", 208, 1));
            operationList.add(new Operation(2, 9, format.parse("2018-05-01"),
                    "Магнит", "Продажа", 209, 1));
            operationList.add(new Operation(1, 10, format.parse("2018-08-20"),
                    "Айкай", "Продажа", 210, 1));
            operationList.add(new Operation(8, 11, format.parse("2018-06-02"),
                    "Оптима", "Продажа", 211, 1));
            operationList.add(new Operation(19, 12, format.parse("2018-07-30"),
                    "Айкай", "Продажа", 212, 1));
            operationList.add(new Operation(1, 13, format.parse("2018-01-07"),
                    "Оптима", "Продажа", 213, 1));
        } catch(ParseException e) {

        }
    }

    void run() {
        operationsRepository.addAll(operationList);

        Operation operation;

        System.out.println("Поиск по ID=1:");
        operation = operationsRepository.finfByID(1L);
        System.out.println(operation.toString());

        System.out.println("Поиск по ID=2:");
        operation = operationsRepository.finfByID(2L);
        System.out.println(operation.toString());

        System.out.println("Поиск по ContractNumber=1:");
        List<Operation> operationsByContractNumber = operationsRepository.findByContractNumber(1);
        for(Operation operation1 : operationsByContractNumber) {
            System.out.println(operation1.toString());
        }

    }
}
