package com.example.misaki.myapp06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class OperationsRepository {
    protected List<Operation> operationsList = new ArrayList<>();
    protected HashMap<Long, Operation> operationsByIdMap = new HashMap<>();
    protected HashMap<Integer, List<Operation>> operationByContractNumberMap = new HashMap<>();
    protected List<Integer> contractNumbersList = new ArrayList<>();

    protected class OperationSortByDateComparator implements Comparator<Operation> {
        @Override
        public int compare(Operation left, Operation right) {
            return left.getDate().compareTo(right.getDate());
        }
    }

    public void addAll(List<Operation> operationList) {
        this.operationsList.addAll(operationList);
        for (Operation operation : operationsList) {
            operationsByIdMap.put(operation.getId(), operation);
            if (!operationByContractNumberMap.containsKey(operation.getContractNumber())) {
                List<Operation> list = new ArrayList<>();
                list.add(operation);
                operationByContractNumberMap.put(operation.getContractNumber(), list);
                contractNumbersList.add(operation.getContractNumber());
            } else {
                List<Operation> list = operationByContractNumberMap.get(operation.getContractNumber());
                list.add(operation);
            }
        }
        for (Integer contractNumber : contractNumbersList) {
            List<Operation> list = operationByContractNumberMap.get(contractNumber);
            Collections.sort(list, new OperationSortByDateComparator());
        }
    }

    Operation finfByID(Long id) {
        return operationsByIdMap.get(id);
    }

    List<Operation> findByContractNumber(Integer contractNumber) {
        return operationByContractNumberMap.get(contractNumber);
    }

}
